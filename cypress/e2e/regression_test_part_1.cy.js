/// <reference types="Cypress" />


import { randomData } from  '../Pages/fakerData'
import data from "../fixtures/sanityData.json"
import * as sourceConnection from "../fixtures/sourceConnection.json"
import * as source from "../pages/sourcesystem_PO"  
import navbarlinks from "../fixtures/leftnavbarlinks.json";
import navbarreportlinks from "../fixtures/leftnavbarreportlinks.json";
import dashboardLinks from "../fixtures/dashboard_links.json";
import * as dashboard from "../Pages/dashboard_PO";
import * as favorite from "../pages/favorite_PO"
import * as gobal from "../Pages/gobal_PO";
import * as setting from "../Pages/setting_PO";
import * as report from "../Pages/report_PO";
import * as biDictionary from "../Pages/biDictonary_PO";
import { checkEmail } from "../utilities/email";
import { checkEmailSubscribe } from "../utilities/email"
import * as reportType from "../Pages/reportType_PO";
import * as categories from "../Pages/categories_PO";
import * as biGlossary from '../Pages/biGlossary_PO'
import * as logins from'../Pages/logins_PO'
import * as categorizedReportPage from '../Pages/categorized_reports_PO'
import * as ldap from "../pages/ldapSetting_PO"
import * as ldapConfiguaration from "../fixtures/ldapConfiguration.json"
import * as reportRating from "../fixtures/report.json"

const glossary_name = 'Actual/Plan Goal'
 
  
describe("Smoke Suite", () => {
  beforeEach(() => {
  
    logins.loginUser2(Cypress.env(),data.login.username,data.login.password);
    
   
  });
  it.skip("TC-00001_Verify the 'Search Field Name' option in BI Glossary Page", () => {
    dashboard.openBiGlossary()
    gobal.verify_SearchField_BIGlossary(glossary_name)  
  });

  it("TC-009_Verify the scroll option in Favourited in Classic Look ", () => {
    dashboard.clickProfile()
    dashboard.dashboardSetting()
    dashboard.toggleDashboardStyle()
    gobal.verify_scrollbottom_fav()
     
  });

  it("TC-010_Verify the 'Recently Viewed' and 'My Reports' links under Reports in classic Look", () => {
    //My Reports  
    dashboard.openMenu()
      dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0])
      dashboard.clickSideMenuLink(data.sidelinks[1].Reports[1])
      cy.wait(5000)
      //Recently Viewed
      dashboard.openMenu()
      dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0])
      dashboard.clickSideMenuLink(data.sidelinks[1].Reports[4])
  })

  it("TC-011_Verify the categories are aligned correctly under 'Categories' in classic Look", () => {
    //My Reports  
      dashboard.openMenu()
      dashboard.clickSideMenuLink(data.sidelinks[3])
      gobal.verify_alignment()
      
  })

  it("TC-012_Verify the three tabs under 'Quick Access' in classic Look", () => {
    gobal.gobalSearch('Pro')
    gobal.verify_quicksearch()

  })

  it("TC-013_Verify thumbnails under Favourites in classic Look", () => {
    dashboard.openMenu()
    dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0])
    dashboard.clickSideMenuLink(data.sidelinks[1].Reports[2])
    gobal.verify_Thumbnailview()
  })

  it.only("TC-050_Verify Hiding and Unhiding reports from Categorized page and ensure it reflects across application ", () => {
    dashboard.openMenu()
    dashboard.clickSideMenuLink(data.sidelinks[1].Reports[0])
    dashboard.clickSideMenuLink(data.sidelinks[1].Reports[1])
    cy.wait(9000)
    cy.get("mat-icon[class='mat-icon notranslate mat-tooltip-trigger review-desc-edit mat-icon-no-color ng-star-inserted']").invoke('click')
  })

  



})