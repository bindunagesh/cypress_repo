
import * as favoriteLocators from "../locators/favorite_locators"

  export function verifyInFavoritesPage(repName){
    cy.wait(2000)
    cy.get(favoriteLocators.sort).click()
    cy.get(favoriteLocators.newestFirst).click()
    cy.get(favoriteLocators.reportName).should('have.text',repName) 
}



