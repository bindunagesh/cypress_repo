import * as biGlossaryLocaters from "../locators/biGlossary_locators";

export function searchField(fieldName) {
  
    cy.get(biGlossaryLocaters.mainPage.searchField).type(fieldName);
    cy.wait(1000)
  }
  export function resultReportTable(reportName) {
    cy.get(biGlossaryLocaters.mainPage.reportResultTable).eq(0).invoke('text').then((searchresult)=>{
      cy.log(searchresult)
        expect(searchresult).to.eq(` ${reportName}`)
      })
  }

  export function searchresult(fieldName){
    // cy.get('.mat-card-content div.mat-tooltip-trigger',{ timeout: 800000 }).eq(6).invoke('text').then((result)=>{
    //   expect(result).to.include(reportName);
    // })
    cy.get('[class="mat-card-content border-line"] div.mat-tooltip-trigger').invoke('text').then((result)=>{
      expect(result).to.include(fieldName)
    })
  }