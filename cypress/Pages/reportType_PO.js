import * as reportTypeLocaters from "../locators/reportTypes_locators";

export function ConnectorType(ConnectorName) {
  
    return cy.get(reportTypeLocaters.mainPage.reportTypes).contains(ConnectorName).click();
  }
  export function sortOrder(selectionType) {
  
    cy.get(reportTypeLocaters.connectorTypePage.sortMenu).click();
    cy.get(reportTypeLocaters.connectorTypePage.sortSelection).contains(selectionType).click()
    cy.get(reportTypeLocaters.connectorTypePage.selectedSortText).invoke('text').then((selection)=>{
    expect(selection).to.eq(selectionType)
    })
  }
  