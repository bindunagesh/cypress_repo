import * as categories from "../locators/categories_locators";

export function mainCategory(ConnectorName) {
  
    return cy.get(categories.mainPage.mainCategoryName).contains(ConnectorName).click();
  }
  export function subCategory(ConnectorName) {
  
    return cy.get(categories.subCategoriesPage.subCategoryName).contains(ConnectorName).click();
  }

  export function sortReports(selectionType) {
  
    cy.get(reportTypeLocaters.connectorTypePage.sortMenu).click();
    cy.get(reportTypeLocaters.connectorTypePage.sortSelection).contains(selectionType).click()
    cy.get(reportTypeLocaters.connectorTypePage.selectedSortText).invoke('text').then((selection)=>{
    expect(selection).to.eq(selectionType)
    })
  }

  export function pinCategory(){
    cy.intercept('GET', '**/analytics-catalog/categories**').as('getCategories');
    cy.get('.simple-page-scroller .icon-content-title').contains('Categories').click();
    cy.wait('@getCategories').then((interception) => {
    const response = interception.response.body;
    const categoryName = response.categories[0].categoryName;
  })
  }
  