// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
import 'cypress-file-upload';
Cypress.Commands.add('login', (username, password) => {
    // cy.session([username, password], () => {
    cy.visit(Cypress.env("BASE_URL"));
    cy.get("#j_username").type(username);
    cy.get("#j_password").type(password);
    cy.get('.zo-login-button').click()
    cy.get('body > app-root > div.main-content.full-height > app-side-nav > div > mat-toolbar').should('exist')
  // })
})

Cypress.Commands.add('loginToTableau', (username, password)=>{
  cy.get('[data-tb-test-id="username-TextInput"]').type(username);
  cy.get('[data-tb-test-id="password-TextInput"]').type(password);
  cy.get('[tb-test-id="button-signin"]').click();

})

//Select Site

Cypress.Commands.add('selectSite', (siteName)=>{
  cy.get(`[data-tb-test-id="list-item-${siteName}"]`).click();
  cy.url().should("include", "/home");

})


//Goto Explore
Cypress.Commands.add('goToExplore',()=>{
  cy.get('[data-tb-test-id="nav-panel"] > ul >:nth-child(9)').click();
  cy.url().should("include", "/explore");
})

Cypress.Commands.add('logMessage', (message) => {
  cy.log(message);
});

Cypress.Commands.add('logToFile', (message) => {
  const logFilePath = 'cypress/logs/test.log';
  Cypress.Browser.Commands.writeFile(logFilePath, `${message}\n`, { log: false });
});



