const reportStatisticsLocators ={

    reportStats:{
        reportstatgraph: '.graph',
        reportstatgraphheader: '.graph-header',
        toggleBackendServer: '.mat-slide-toggle-thumb',
        overallradiobtn: '.mat-radio-outer-circle',
        usagesourceTableau: '.ng-tns-c290-1289.ng-trigger.ng-trigger-animationState.ng-star-inserted',
        bargraph: '.bar',
        reportname: '.mat-tooltip-trigger.zo-report-name',
        closebtn: '.mat-icon.notranslate.zo-close.for-icon-close.material-icons.mat-icon-no-color',
        searchbox: '.search-block.ng-star-inserted',
        openreport: '.data-title',
        graphheader: '.graph-header',
        reportheader: '.header-text.ng-star-inserted',
        graph: '.graph'

    }

}
module.exports =reportStatisticsLocators