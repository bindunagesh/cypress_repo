const reportTypeLocators ={
  
    mainPage:{
     
        reportTypes : '.zo-main-reportlist-container p.name',
        
        },

    connectorTypePage:{

        sortMenu : '.zo-sort-section>div.zo-sort',
        sortSelection : '.zo-sort-section div.sort-container div.sort-list-item',
        selectedSortText :'.zo-sort-section>.zo-sort>.zo-sort-text'
       
}


}
module.exports = reportTypeLocators