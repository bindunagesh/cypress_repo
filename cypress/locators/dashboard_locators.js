const dashboardLocators ={

  homeIcon: '.zo-home-icon',
  cardTitleText:'smooth-dnd-draggable span.card-title-text',
  favoriteText:/^\s*\n?Favorites$/,
  zoomslider:'app-zoomslider',
  carouselElement:'app-zoomslider.carousel-element.ng-star-inserted',
  favDisplay:'.fav-display-value',
  notification:'[mattooltip="Notifications"]',
  notificationPrivateComment: ':nth-child(1) > .notification-link',
  profile:'[mattooltip="Profile"]',
  dashboardSetttingicon:'.zo-profile-common [data-mat-icon-name="dashboard_settings_icon"]',
  logout: 'span.side-menu-icon-text',
  clickPrivateComment:':nth-child(1) > .link-item > .link-text > :nth-child(1)',
  contentTitle: '.simple-page-scroller .icon-content-title',
  homeButton:'#Home',
  categoryTitle:'.categorys-card',
  categoryHeader:'[data-mat-icon-name="ellipsis-icon"]',
  categorypin:'.pin-workflow-dropdown>a',
  reportheaders: '.zo-common-header-left-text',
  dashboardheader: '.zo-route-link.zo-events-none.ng-star-inserted',
  



    leftNavMenu:{
     
        getMenu : '.sidenav-header [svgicon="Menu"]',
        getAllLinksText : '.mat-drawer [class="mat-list-item-content"]',
        getReportSubMenu : '.show-submenu',
        reportMenu : '.sidenavListItem'

    }








}
module.exports =dashboardLocators