const reportLocators ={

    report:{
        getReportRatingComment : '#writeComment',
        getRating : '.rating-icons > app-star-rating > .ng-star-inserted',
        getReportSubmit : '.primary-btn',
        getReportAbout : '#mat-chip-list-0 > .mat-chip-list-wrapper > :nth-child(2) > span.ng-star-inserted',
        getReportPublicComments : '.selected > span.ng-star-inserted',
        getReportComment : '.mat-chip-list-wrapper > :nth-child(4) > span.ng-star-inserted',
        getWriteComment : '.text-area-content', 
        getCommentButton : '.comment-button',
        getsearchInput: '.serach-input > .ng-pristine',
        getsearchUserList: '.search-userList > :nth-child(1) > span',
        getPrivate : '.public-private-container > :nth-child(2)',
        getMyWorkspaceSaveButton : ':nth-child(1) > .workspace-action-label',
        getMyWorkspaceNextButton : '.zo-save-button > .mat-button-wrapper',
        getMyWorkspaceNextButton2: "button[class='mat-focus-indicator zo-save-button mat-button mat-flat-button mat-button-base _mat-animation-noopable ng-star-inserted']",
        getMyWorkspaceEditButton: "mat-icon[data-mat-icon-name='Edit_icon']",
        getMyWorkspaceAddMoreButton: ".add-more-btn",
        getMyWorkspacecheckbox: "span[class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin']",
        getMyWorkspaceSearch: "input[data-placeholder='Search Reports']",
        getMyWorkspaceAdd: ".add-btn",
        getMyWorkspaceRemove: "mat-icon[data-mat-icon-name*='action_close']",
        getMyWorkspaceUpdateBtn: "button[class='save-btn']",
        getMyWorkspaceShareBtn: "span[class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin']",
        input_user: "input[name='username']",
        input_userlist: "div[class='ng-star-inserted']",
        getMyWorkspaceMultiView : '',
        getMyWorkspaceGenerate : '',
        getWorkflowName : '.zo-header-left > :nth-child(1) > .zo-text-box',
        getWorkflowDescription : '.zo-header-right > div > .zo-text-box',
        getWorkflowShareable : '#mat-radio-2 > .mat-radio-label > .mat-radio-container',
        getGenerate : ':nth-child(3) > .workspace-action-label',
        getNext : '.zo-save-button > .mat-button-wrapper',
        getUserName : '.zo-content-username > .mat-form-field > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-infix',
        getPassword : '.zo-content-password > .mat-form-field > .mat-form-field-wrapper > .mat-form-field-flex > .mat-form-field-infix',
        getSave : '.zo-save-generate-btn > .mat-button-wrapper',
        getGenerateOnCredentials : '.zo-save-generate-btn > .mat-button-wrapper',
        searchInput : '.search-input > .ng-untouched',
        userList : '.search-userList > .ng-star-inserted > span',
        sendEmailIcon : '.icon[mattooltip="Send Mail"]',
        writeEmailRecipient :'.mail-content .to-input',
        writeEmailMessage :'.mail-content .text-area-content',
        sendEmailButton :'.send-button',
        reportTitle : '.report-title',
        reportHeader : '[class="reports"] >section div .report-info',
        reportHeaderBottomActionIcon :'[class="align-action-icons-bottom"]',
        confirmationDailogForFav : '.text',
        confirmationForYes :'.actions > .zenButton > .mat-button-wrapper',
        reportToastnotfication: '#toast-container>div>div',
        reportToasterNotification : '#toast-container>div>div',
        reportRatingText :'.review-left-section > .subtext-icons > .zo-star-text',
        reviewContent :'.review-content',
        allComments : '.all-tab-header > span',
        workflows : "span[class='mat-tooltip-trigger flow-name']", // added
        workflowName : "span[class='mat-tooltip-trigger flow-name']",
        deleteWorkflow : ':nth-child(1) > .flow-card > .flow-list-wrapper > .flow-footer > .flow-footer-right > [mattooltip="Delete Workflow"]',
        deleteWorkflowButton : '.zo-btn-delete > .mat-button-wrapper',
        saveWorkflowButton : "button[class='mat-focus-indicator zo-save-button mat-button mat-flat-button mat-button-base _mat-animation-noopable ng-star-inserted'] span[class='mat-button-wrapper']",
        workflowToasterNotification : '#toast-container>div>div',
        thumbnailLog:'#mat-tab-label-1-1 > .mat-tab-label-content',
        thumbnailStatus : ':nth-child(1) > .cdk-column-jobStatus > .thumb-job-status-text',
        numberOfWFTab : '.tab-header > .ng-star-inserted',
        clickcomment: 'body > app-root:nth-child(1) > div:nth-child(1) > app-side-nav:nth-child(1) > div:nth-child(1) > mat-toolbar:nth-child(1) > mat-toolbar-row:nth-child(1) > div:nth-child(2) > div:nth-child(2) > span:nth-child(2) > div:nth-child(3) > div:nth-child(1) > app-links-side-nav:nth-child(1) > div:nth-child(1) > mat-accordion:nth-child(1) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(1) > span:nth-child(1)',
        commentCreator: "span[class='comment-creator']",
        commentMessage:':nth-child(1) > .level-comment > .comment-right > :nth-child(2) > .comment-message',
        search: "input[name='username']",
        sharedwithme: '.tab-header.ng-star-inserted',
        sharecount: '.share-count',
        sharewf: '.mat-icon.notranslate.mat-tooltip-trigger.mat-icon-no-color.ng-star-inserted',
        certifiedBox: '#mat-checkbox-1',
        businessOwner: '#mat-input-1',
        workflownames: '.zo-desc-text',
        workflow_info: "mat-icon[mattooltip='Workflow Info']",
        workflow_infoames: "span[class='mat-tooltip-trigger users-element label-value']",
        email_sub: "input[name='message']",
        shr_btn: '.share-btn',
        share_alert: "div[role='alert']",
        dashboard_div: "div[class='zoomslider-reports-container']",
        selectmostlyviwedreport: "div[class='zoomslider-wrap ng-star-inserted']",
        viewcount: "span[class='zo-thumb-footer-text']",
        favcard_report: "div[class='thumbnail-card-shadow']",
        list_viewcount: "span[class='zo-user-count']",
        grid_viewcount: "span[class='zo-star-text']",
        list_card: "mat-card[class='mat-card mat-focus-indicator mat-tooltip-trigger zo-report-list-card action-card IE-flex-auto _mat-animation-noopable ng-star-inserted']",
        grid_card: "mat-card[class='mat-card mat-focus-indicator mat-tooltip-trigger zo-report-grid-card action-card _mat-animation-noopable ng-star-inserted']",
        grid_categories: "div[class='zo-grid-details']",
        report_types: "mat-card[class='mat-card mat-focus-indicator zo-main-reportlist-card action-card _mat-animation-noopable ng-star-inserted']",
        search_cat: "div[class='search-block']",
        home: "span[class='ng-star-inserted']",
        cerify_checkbox: '.mat-checkbox-inner-container',
        certify_custom: ".custom-style-for-mat-inputs",
        certify_level: "mat-option[role='option']",
        certify_savebtn: '.common-modal__footer--save-btn',
        zo_dec: "mat-icon[class='mat-icon notranslate mat-tooltip-trigger review-desc-edit mat-icon-no-color ng-star-inserted']",
        zo_des_textarea: '.zen-desc.zen-text-area.ng-untouched.ng-pristine.ng-valid',
        bussiness_owner: "mat-icon[mattooltip='Edit Business Owner']",
        bussiness_owner_textarea: ".search-user-container.ng-star-inserted",
        business_userlist: "span[class='user-list-cursor']",
        comments: "#writeComment",
        thumbnail_card: "div[class='thumbnail-card ng-star-inserted']",
        certify_icon: "mat-icon[svgicon='CertificationIcon_new']",
        info_btn: "mat-icon[data-mat-icon-name='info_active']",
        info_businessowner: "span[class='mat-tooltip-trigger fav-thumb-details-value']",
        info_description: "span[class='fav-thumb-details-value']",
        reportcategory: ".mat-tooltip-trigger.text-link.text-ellipsis-for-report-cat-hierarchy",
        info_category: "div[class='thumbnail-img ng-star-inserted'] div:nth-child(2) div:nth-child(1) span:nth-child(2)",
        report_type_info: "span[class='value']",
        info_report_type: "body > app-root:nth-child(1) > div:nth-child(1) > app-side-nav:nth-child(1) > div:nth-child(1) > mat-sidenav-container:nth-child(2) > mat-sidenav-content:nth-child(5) > mat-sidenav-container:nth-child(1) > mat-sidenav-content:nth-child(2) > div:nth-child(1) > section:nth-child(1) > app-list:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > section:nth-child(1) > div:nth-child(1) > div:nth-child(1) > app-thumbnail-card:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(3) > span:nth-child(2)",
        info_source_system: "body app-root div[class='zo-grid-descripion-pagination fav-description-scroll'] div div:nth-child(2) span:nth-child(2)",
        report_owner: "section[class='cdk-drag report-view cdk-drag-disabled ng-star-inserted'] span[class='owner-link']",
        info_report_owner: "div[class='thumbnail-card ng-star-inserted scaled-favorites-card'] div:nth-child(6) span:nth-child(2)",
        sort: "div[class='zo-common-header-right ng-star-inserted'] span:nth-child(2)",
        sort_items: "div[class='sort-list-item ng-star-inserted']",
        dashreport: ".zoomslider-wrap.ng-star-inserted",
        dash_infobtn: "mat-icon[data-mat-icon-name='info_active']"


    }

}
module.exports =reportLocators