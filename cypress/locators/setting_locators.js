const settingLocators = {
  adminSettings: {
    administratorSettings:
      '[class="side-menu-icon-text left-side-sub-menu ng-star-inserted"]',
    PreviousNextButton: '.mat-button-wrapper',
    ConnectionName: '.mat-cell.cdk-cell.cdk-column-SourceName.mat-column-SourceName.ng-star-inserted',
    JobHistory: "mat-icon[mattooltip='Job History']",
    userid: ".mat-cell.cdk-cell.cdk-column-UserId.mat-column-UserId.ng-star-inserted",
    connectionnameid: ".mat-cell.cdk-cell.cdk-column-SourceName.mat-column-SourceName.ng-star-inserted",
    reportnameid: ".mat-cell.cdk-cell.cdk-column-DISPLAY_VALUE.mat-column-DISPLAY_VALUE.ng-star-inserted",
    displayvalueid: ".mat-cell.cdk-cell.cdk-column-Display_Value.mat-column-Display_Value.ng-star-inserted",
      reportTypes:{
        reporttypesetting:'.ng-star-inserted>mat-list>:nth-child(3)',
        searchReports:'.zo-users-header mat-icon[mattooltip="Search Report Type(s)"]',
        searchRow:'.zo-table .mat-row',
        editRow:'.zo-table .mat-row>td:nth-child(5) mat-icon',
        checkBox:'[formarrayname="ConnectionParams"] .mat-checkbox-layout',
        inputbox: '.mat-form-field input',
        submitButton:'.zo-userForm-button>button',
        menuList:'.side-menu-scroll',
      catPage: '.mat-list-item-content', //':nth-child(4) > .mat-list-item-content',
      getReportAbout : '#mat-chip-list-0 > .mat-chip-list-wrapper > :nth-child(2) > span.ng-star-inserted',
      

      


      },
      userSettings:{
        addUserButton:'[data-mat-icon-name="Global_Add"]',
        userIdInput :'.mat-form-field-infix>input#mat-input-0',
        userPassInput:'.mat-form-field-infix>#mat-input-2',
        userPassConfirmInput:'.mat-form-field-infix>#mat-input-3',
        userFirstNameInput:'.mat-form-field-infix>#mat-input-4',
        userLastNameInput:'.mat-form-field-infix>#mat-input-5',
        userEmailInput:'.mat-form-field-infix>#mat-input-6',
        savebutton:'.zo-btn-success',
        Notification : '#toast-container>div>div',

        
      },
    applicationSettings: {
      getApplicationSetting: '[class="side-menu-icon-text sub-menu-text"]',
      getsourceconnectionsetting:'.ng-star-inserted>mat-list>:nth-child(2)',
      uploadCompanyLogo:'.zo-change-logo-container>input',
      uploadCompanyLogoClass : 'div.zo-new-logo-container .zo-logo-image-container',
      uploadCompanyLogoButton: '.zo-change-logo-container .mat-card-content div.zo-action-text',
      uploadCompanyLogoToastNotification : '#toast-container>div>div',
      customattributes: '#mat-tab-label-0-3',
      toggleRG: '.mat-slide-toggle-label',
      updatebtn: '.zo-customattr-section > :nth-child(3)',
      thumbnailattributes: '.mat-tab-label-content',
      togglethumbnailattr: '.mat-slide-toggle-label',
      thumbnailupdatebtn: 'mat-tab-body button:nth-child(2)',
      toastmsg: "div[aria-label='Thumbnail attributes updated successfully']",
      systemflag: ".zo-btn-default",
      dashboardstyle: "span[class*='mat-select-min-line ng-tns-c137-22 ng-star-inserted']",
      style: 'mat-option',
      toaststyle: "div[aria-label='Successfully updated']",
      closestyle: ".mat-icon.notranslate.action-element.zo-close-icon.material-icons.mat-icon-no-color",
      styleswitch: "mat-select",
      dashstyle: "mat-option",


      thumbnailSetting: {
        tab: ".mat-ripple>.mat-tab-label-content",
        search: '[placeholder="Search Reports"]',
        searchcard: '[class="mat-row cdk-row ng-star-inserted"]',
        searchcardReportName:'[class="mat-row cdk-row ng-star-inserted"] td:nth-child(2)',
        chooseThumbnailIcon:
          'table tbody[role="rowgroup"] >tr :nth-child(8)>input',
        deleteUploadedThumbnail:
          'table tbody[role="rowgroup"] >tr :nth-child(8)>mat-icon[mattooltip="Delete Uploaded Thumbnail"]',
        uploadbutton: ".attr-buttons-container button",
        actionColumn: 'table tbody[role="rowgroup"] >tr :nth-child(8)',
        stateColumn: ".cdk-column-imageStatus > span",
        checkbox: '[class="mat-row cdk-row ng-star-inserted"]>td>mat-checkbox>label',
        statusColumn: '[class="thumb-job-status-text"]',
        thumbnailDeleteConfirmation: ".zo-btn-delete",
      },
     
      ldapConfiguaration: {
        tab: ".mat-ripple>.mat-tab-label-content",
        ldapCheckbox : ".mat-checkbox-inner-container > input",
        ldapClickCheckbox : ".mat-checkbox-inner-container ",
        ldapServerUrl1: '[formcontrolname="ldapserverurl"]',
        testConnection : "[class='mat-focus-indicator zo-green-button mat-button mat-flat-button mat-button-base _mat-animation-noopable']",
        ldapConfigParent : '[class="ldap-config-wrap"]',
        addAdditionalLdapurl : '[mattooltip="Add addtional LDAP url"]',
        removeLdapUrl: '[mattooltip="Remove the LDAP url"]',
        ldapServerUrl2: '[formcontrolname="ldapnewserverurlname"]',
        ldapbinddn :'[formcontrolname="ldapbinddn"]',
        ldappassword :'[formcontrolname="ldappassword"]',
        ldapuserbase :'[formcontrolname="ldapsserbase"]',
        ldapattributes :'[formcontrolname="ldapattributes"]',
        updateButton :'.zo-buttons-container > button'
        

      },

      reportOpen:{
        searchReportInMyReport: '.paginated-content'  //.zo-report-grid-container',
        //searchReportInMyReport: '.thumbnail-reports-container',
        
  
      },

      
  
  

    },
  },
};
module.exports = settingLocators;
